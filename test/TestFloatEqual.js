module("Float Equal");

test ("Preconditions", function() {
	throws(SigmaIntegral.floatEqual (null, 0.0, 0.0), SigmaIntegral.ArgumentException, "f1 not a number was allowed");
	throws(SigmaIntegral.floatEqual (0.0, null, 0.0), SigmaIntegral.ArgumentException, "f2 not a number was allowed");
	throws(SigmaIntegral.floatEqual (0.0, 0.0, null), SigmaIntegral.ArgumentException, "maxRelativeError not a number was allowed");
});

test ("0.0 equal 0.0", function() {
	ok(SigmaIntegral.floatEqual(0.0, 0.0, 0.0), "Both numbers literal zero should return true");
});

test ("Literals are equal (6.1)", function() {
	ok(SigmaIntegral.floatEqual(6.1, 6.1, 0.0), "Literal numbers (6.1) not returning equal");
});

test ("Literals not equal (6.1, 0.0)", function() {
	ok(SigmaIntegral.floatEqual(6.1, 0.0, 0.0), "Literal numbers returning equal when not");
});

test ("Equivalent operations return aproximately equal results (12.2/2.0 and 2.5+3.6)", function() {
	ok(SigmaIntegral.floatEqual(12.2/2.0, 2.5+3.6, 0.001), "Equivalent operation returning false with maxRelativeError of 0.001");
});

test ("Non-Equivalent operations return non-equal results (12.2*2.0 and 2.5+3.6)", function() {
	ok(SigmaIntegral.floatEqual(12.2*2.0, 2.5+3.6, 0.001), "Non-Equivalent operation returning true with maxRelativeError of 0.001");
});
