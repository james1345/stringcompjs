var SigmaIntegral = (typeof SigmaIntegral === "undefined" ? {} : SigmaIntegral);

SigmaIntegral.ArgumentException = function(message) {
	if(typeof message != "string"){
		throw new SigmaIntegral.ArgumentException("Message must be a string!");
	}
	this.message = message;
};

SigmaIntegral.ArgumentException.prototype.toString = function() {
	return this.message;
}
