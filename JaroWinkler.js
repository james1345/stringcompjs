var SigmaIntegral = {	
	
	MatchChar: function(character){
		if(typeof character != "string" || character.length != 1){
			throw new SigmaIntegral.ArgumentException("Must pas a single character for conversion!");
		}
		this.char = character;
		this.aligned = false;
	},
	
	stringToMatchArray: function(string){
		var self = this;
		if(typeof string != "string"){
			throw new self.ArgumentException("Argument must be a string!");
		}
		var matchArray = [];
		for(var i = 0; i < string.length; i++){
			matchArray.push(new self.MatchChar(string.charAt(i)));
		}
		return matchArray;
	},
	
	matches: function(string1, string2){
		var self = this;
		if(typeof string1 != "string"){
			throw new self.ArgumentException("String1 must be a string!");
		}
		if(typeof string2 != "string"){
			throw new self.ArgumentException("String2 must be a string!");
		}
		
		var MatchTableRow = function(c1, i1, c2, i2){
			this.char1 = c1;
			this.index1 = i1;
			this.char2 = c2;
			this.index2 = i2;
		};
		var matchTable = [];
		
		var matchThreshold = Math.floor(Math.max(string1.length, string2.length)/2) - 1;
		var s1 = self.stringToMatchArray(string1);
		var s2 = self.stringToMatchArray(string2);
		
		var i = 0;
		
		for(i = 0; i < string1.length; i++){
			var j = 0;
			for(j = Math.max(i - matchThreshold, 0); j < Math.min(i+matchThreshold+1, s2.length); j++){
				if(s1[i].char == s2[j].char && !(s1[i].aligned) && !(s2[j].aligned)){
					s1[i].aligned = true;
					s2[j].aligned = true;
					matchTable.push(new MatchTableRow(s1[i].char, i, s2[j].char, j));
				}
			}
		}
		return matchTable;
	},
	
	jaro: function(string1, string2){
		var self = this;
		if(typeof string1 != "string"){
			throw new self.ArgumentException("String1 must be a string!");
		}
		if(typeof string2 != "string"){
			throw new self.ArgumentException("String2 must be a string!");
		}
		
		var jaro = 0;
		
		var matchTable = self.matches(string1, string2);
		var m = matchTable.length;
		
		if(m > 0){
			var t = 0.0;
			for(var i = 0; i < m; i++) {
				if(matchTable[i].index1 != matchTable[i].index2){
					t += 0.5;
				}
			}
			t = Math.floor(t);
			jaro = ((m/string1.length)+(m/string2.length)+((m - t)/m))/3;
		}
		return  jaro;
	},
	
	jaroWinkler: function(string1, string2, boostThreshold, prefixLength){
		
		var self = this;
		
		if(typeof string1 != "string"){
			throw new self.ArgumentException("String1 must be a string!");
		}
		if(typeof string2 != "string"){
			throw new self.ArgumentException("String2 must be a string!");
		}
		if(typeof boostThreshold != "number"){
			throw new self.ArgumentException("boostThreshold must be a number");
		}
		if(typeof prefixLength != "number"){
			throw new self.ArgumentException("prefixLength must be a number");
		}
		
		if(0.0 > boostThreshold || 1.0 < boostThreshold){
			throw new self.ArgumentException("boostThreshold must be between 0.0 and 1.0");
		}
		if(0 > prefixLength){
			throw new self.ArgumentException("prefixLength must be >= 0")
		}
		
		prefixLength = ~~prefixLength; //Fast floor for +ve numbers
		
		var p = 0;
		var match = true;
		var jwScore = self.jaro(string1, string2);
		
		if(jwScore > boostThreshold){
			while(p < prefixLength && match){
				if(string1.charAt(p) == string2.charAt(p)){
					p++;
				}
				else {
					match = false;
				}
			}
			jwScore = jwScore + (0.1 * p *(1-jwScore));
		}
		return jwScore;
	}
};
