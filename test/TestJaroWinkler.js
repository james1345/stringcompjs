module("Jaro-Winkler Proximity");

test( "Jaro", function(){
	ok(SigmaIntegral.jaro("Cat", "Cat") == 1, "Jaro dunt run");
});

test( "Jaro-Winkler", function() {
	ok(SigmaIntegral.jaroWinkler("Cat", "Cat", 0.7, 4) == 1, "Jaro dunt run");
});
